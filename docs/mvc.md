# The Model View Controller Pattern

Example : 
```   
GET /about HTTP/1.1
Host: 127.0.0.1
```

## Routes
Matchers for the URL that is requested

GET for "/about" --> requested "/about" handled by the AboutController

## Models
Database wrapper
User
* query for records
* wrap individual records

## Views
Response body content sent back to the browser and displayed :
* HTML
* CSV
* PDF
* XML

## Controllers
Decide how to process a request and define a response. 